--������ ��� ������� ��������� ������� ����� �� ������������ ������ �������
with
    param as
        (
            select
                to_date('01.06.2019') in_dt_bkg_from --���� ������ ������������ ������ ��������
                , to_date('10.06.2019') in_dt_bkg_to --���� ��������� ������������ ������ ��������
            from
                dual
        )
    --������� ���� ����������������� ������� �� ��������� ������
    , crm_tbl as
        (
            select
                *
            from
                hotel.client_rel_room crm
                , param
            where
                crm.date_from <= param.in_dt_bkg_to
                and crm.date_to >= param.in_dt_bkg_from
        )
select
    *
from
    hotel.room rm
where
    rm.id not in (select ct.room_id from crm_tbl ct)
order by
    rm.strcode
