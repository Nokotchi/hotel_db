--
-- Create Schema Script 
--   Database Version   : 11.2.0.3.0 
--   Toad Version       : 10.5.0.41 
--   DB Connect String  : MAN 
--   Schema             : HOTEL 
--   Script Created by  : W_ALL 
--   Script Created at  : 27.06.2019 8:31:55 
--   Physical Location  :  
--   Notes              :  
--

-- Object Counts: 
--   Roles: 1           Sys Privs: 8        Roles: 0            Obj Privs: 0 
--   Users: 1           Sys Privs: 6        Roles: 1            Tablespace Quotas: 1 
--   Tablespaces: 2     DataFiles: 1        TempFiles: 1 
-- 
--   Directories: 3 
--   Indexes: 5         Columns: 7          
--   Packages: 1        Lines of Code: 32 
--   Package Bodies: 1  Lines of Code: 172 
--   Sequences: 3 
--   Tables: 3          Columns: 10         Partitions: 1       Constraints: 6      
--   Triggers: 3 


-- "Set define off" turns off substitution variables. 
Set define off; 

--
-- TEMP  (Tablespace) 
--
CREATE TEMPORARY TABLESPACE TEMP TEMPFILE 
  '/oradata/app/oracle/oradata/man/temp01.dbf' SIZE 5643M AUTOEXTEND ON NEXT 640K MAXSIZE UNLIMITED
TABLESPACE GROUP ''
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;


--
-- USERS  (Tablespace) 
--
CREATE TABLESPACE USERS DATAFILE 
  '/oradata/app/oracle/oradata/man/users01.dbf' SIZE 14572M AUTOEXTEND ON NEXT 1M MAXSIZE UNLIMITED
LOGGING
ONLINE
PERMANENT
EXTENT MANAGEMENT LOCAL AUTOALLOCATE
BLOCKSIZE 8K
SEGMENT SPACE MANAGEMENT AUTO
FLASHBACK ON;


--
-- "DEFAULT"  (Profile) 
--
CREATE PROFILE "DEFAULT" LIMIT
  SESSIONS_PER_USER UNLIMITED
  CPU_PER_SESSION UNLIMITED
  CPU_PER_CALL UNLIMITED
  CONNECT_TIME UNLIMITED
  IDLE_TIME UNLIMITED
  LOGICAL_READS_PER_SESSION UNLIMITED
  LOGICAL_READS_PER_CALL UNLIMITED
  COMPOSITE_LIMIT UNLIMITED
  PRIVATE_SGA UNLIMITED
  FAILED_LOGIN_ATTEMPTS 10
  PASSWORD_LIFE_TIME UNLIMITED
  PASSWORD_REUSE_TIME UNLIMITED
  PASSWORD_REUSE_MAX UNLIMITED
  PASSWORD_LOCK_TIME 1
  PASSWORD_GRACE_TIME 7
  PASSWORD_VERIFY_FUNCTION NULL;


--
-- RESOURCE  (Role) 
--
CREATE ROLE RESOURCE NOT IDENTIFIED;

GRANT CREATE TRIGGER TO RESOURCE;
GRANT CREATE SEQUENCE TO RESOURCE;
GRANT CREATE TYPE TO RESOURCE;
GRANT CREATE PROCEDURE TO RESOURCE;
GRANT CREATE CLUSTER TO RESOURCE;
GRANT CREATE OPERATOR TO RESOURCE;
GRANT CREATE INDEXTYPE TO RESOURCE;
GRANT CREATE TABLE TO RESOURCE;


--
-- HOTEL  (User) 
--
CREATE USER HOTEL
  IDENTIFIED BY <password>
  DEFAULT TABLESPACE USERS
  TEMPORARY TABLESPACE TEMP
  PROFILE DEFAULT
  ACCOUNT UNLOCK;
  -- 1 Role for HOTEL 
  GRANT RESOURCE TO HOTEL;
  ALTER USER HOTEL DEFAULT ROLE ALL;
  -- 6 System Privileges for HOTEL 
  GRANT CREATE ANY SEQUENCE TO HOTEL;
  GRANT CREATE SESSION TO HOTEL;
  GRANT CREATE ANY TABLE TO HOTEL;
  GRANT CREATE TRIGGER TO HOTEL;
  GRANT CREATE PROCEDURE TO HOTEL;
  GRANT UNLIMITED TABLESPACE TO HOTEL;
  -- 1 Tablespace Quota for HOTEL 
  ALTER USER HOTEL QUOTA UNLIMITED ON USERS;


--
-- ORACLE_OCM_CONFIG_DIR  (Directory) 
--
CREATE OR REPLACE DIRECTORY 
ORACLE_OCM_CONFIG_DIR AS 
'/oradata/app/oracle/db1/ccr/state';


--
-- DATA_PUMP_DIR  (Directory) 
--
CREATE OR REPLACE DIRECTORY 
DATA_PUMP_DIR AS 
'/oradata/app/oracle/admin/man/dpdump/';


--
-- XMLDIR  (Directory) 
--
CREATE OR REPLACE DIRECTORY 
XMLDIR AS 
'/oradata/app/oracle/db1/rdbms/xml';


--
-- SEQCLIENT  (Sequence) 
--
CREATE SEQUENCE HOTEL.SEQCLIENT
  START WITH 5
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


--
-- SEQCLIENT_REL_ROOM  (Sequence) 
--
CREATE SEQUENCE HOTEL.SEQCLIENT_REL_ROOM
  START WITH 3
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


--
-- SEQROOM  (Sequence) 
--
CREATE SEQUENCE HOTEL.SEQROOM
  START WITH 7
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


--
-- CLIENT  (Table) 
--
CREATE TABLE HOTEL.CLIENT
(
  ID   NUMBER(20)                               NOT NULL,
  FIO  VARCHAR2(200 BYTE)                       NOT NULL
)
TABLESPACE USERS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON TABLE HOTEL.CLIENT IS '������� � ����������� � ��������';


--
-- ROOM  (Table) 
--
CREATE TABLE HOTEL.ROOM
(
  ID       NUMBER(20)                           NOT NULL,
  STRCODE  VARCHAR2(20 BYTE)                    NOT NULL
)
TABLESPACE USERS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );

COMMENT ON TABLE HOTEL.ROOM IS '������� � ����������� � ������� � �����';


--
-- CLIENT_PK  (Index) 
--
CREATE UNIQUE INDEX HOTEL.CLIENT_PK ON HOTEL.CLIENT
(ID)
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


--
-- ROOM_PK  (Index) 
--
CREATE UNIQUE INDEX HOTEL.ROOM_PK ON HOTEL.ROOM
(ID)
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


--
-- TBIR_ROOM_SET_ID  (Trigger) 
--
CREATE OR REPLACE TRIGGER HOTEL.TBIR_ROOM_SET_ID
BEFORE INSERT
ON HOTEL.ROOM REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.id IS NULL
      )
BEGIN
    SELECT hotel.SEQROOM.NEXTVAL INTO :NEW.id FROM dual;
END;
/


--
-- TBIR_CLIENT_SET_ID  (Trigger) 
--
CREATE OR REPLACE TRIGGER HOTEL.TBIR_CLIENT_SET_ID
BEFORE INSERT
ON HOTEL.CLIENT 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.id IS NULL
      )
BEGIN
    SELECT hotel.SEQCLIENT.NEXTVAL INTO :NEW.id FROM dual;
END;
/


--
-- CLIENT_REL_ROOM  (Table) 
--
CREATE TABLE HOTEL.CLIENT_REL_ROOM
(
  ID         NUMBER(20)                         NOT NULL,
  CLIENT_ID  NUMBER(20)                         NOT NULL,
  ROOM_ID    NUMBER(20)                         NOT NULL,
  DATE_FROM  DATE                               NOT NULL,
  DATE_TO    DATE                               NOT NULL,
  FLAG_VIP   NUMBER(1)
)
TABLESPACE USERS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
PARTITION BY RANGE (DATE_FROM)
INTERVAL( INTERVAL'1'YEAR)
(  
  PARTITION CL_REL_RM_YEAR2019 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    TABLESPACE USERS
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
);

COMMENT ON TABLE HOTEL.CLIENT_REL_ROOM IS '������ � ����������� � ������������ �������';


--
-- CLIENT_REL_ROOM_PK  (Index) 
--
CREATE UNIQUE INDEX HOTEL.CLIENT_REL_ROOM_PK ON HOTEL.CLIENT_REL_ROOM
(ID)
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


--
-- IDX_CL_REL_RM_ROOM_DT_FROM_TO  (Index) 
--
CREATE INDEX HOTEL.IDX_CL_REL_RM_ROOM_DT_FROM_TO ON HOTEL.CLIENT_REL_ROOM
(ROOM_ID, DATE_FROM, DATE_TO)
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );


--
-- PROC  (Package) 
--
CREATE OR REPLACE PACKAGE HOTEL.PROC AS
--����� ��������� ��� ����������� �������� ��������� � ��������������� �������

    --������� ���������� ��� ������� ���������� � ��������� ������ �� ��������������
    function get_romm_row_by_id(in_room_id hotel.room.id%type)
    return hotel.room%rowtype;

    --������� ���������� ��� ������� ���������� � ������� �� ��������������
    function get_client_row_by_id(in_client_id hotel.client.id%type)
    return hotel.client%rowtype;

    --������� ��� �������� ������� ���������� ��� ������ �� ��������� ������    
    function is_room_booked(in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return NUMBER;

    --������� ��� ������� ������� VIP-������� �� ������ ���������� api
    function get_client_status_from_api(in_client_id hotel.client.id%type)
    return NUMBER;    
    
    --������� ��� ������� ������������ ������ � �����
    function add_client_rel_room(in_client_id hotel.client.id%type, in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return hotel.client_rel_room.id%type;
    
    --������� ���������� ��� ������� ������������� ������ � ������� ������ �� ���������� �������������� ������, �������������� ������� � ��������� ����� ������������
    function get_client_rel_room_id_by_inf(in_client_id hotel.client.id%type, in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return hotel.client_rel_room.id%type;
    
    --������� ��� ������� ������ ������������ ������ �� ��������� ������
    function del_client_rel_room(in_client_id hotel.client.id%type, in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return hotel.client_rel_room.id%type;    
    
END PROC;
/


--
-- PROC  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY HOTEL.PROC AS
--����� ��������� ��� ����������� �������� ��������� � ��������������� �������
    
    --������� ���������� ��� ������� ���������� � ��������� ������ �� ��������������
    function get_romm_row_by_id(in_room_id hotel.room.id%type)
    return hotel.room%rowtype
    is
        room_row hotel.room%rowtype;
    begin
        select
            *
        into
            room_row
        from
            hotel.room r
        where
            r.id = in_room_id;
        return room_row;
        exception
            when NO_DATA_FOUND then
                return null;
    end;

    --������� ���������� ��� ������� ���������� � ������� �� ��������������
    function get_client_row_by_id(in_client_id hotel.client.id%type)
    return hotel.client%rowtype
    is
        client_row hotel.client%rowtype;
    begin
        select
            *
        into
            client_row
        from
            hotel.client c
        where
            c.id = in_client_id;
        return client_row;
        exception
            when NO_DATA_FOUND then
                return null;
    end;

    --������� ��� �������� ������� ���������� ��� ������ �� ��������� ������    
    function is_room_booked(in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return NUMBER
    is
        crm_cnt NUMBER(20);
    begin
        select
            count(1)
        into
           crm_cnt
        from
            hotel.client_rel_room crm
        where
            crm.room_id = in_room_id
            and crm.date_from <= in_dt_bkg_to
            and crm.date_to >= in_dt_bkg_from;
        return crm_cnt;
    end;

    --������� ��� ������� ������� VIP-������� �� ������ ���������� api
    function get_client_status_from_api(in_client_id hotel.client.id%type)
    return NUMBER
    is
        client_row hotel.client%rowtype;
        req UTL_HTTP.req;
        resp  UTL_HTTP.resp;
        api_url VARCHAR2(200) := 'http://localhost/get_client_status.php';
        req_param VARCHAR2(200) := 'client_fio=';
        val VARCHAR2(1);
    begin
        client_row := get_client_row_by_id(in_client_id);
        req := UTL_HTTP.begin_request(api_url);
        UTL_HTTP.set_header(req, 'User-Agent', 'Mozilla/4.0');
        UTL_HTTP.write_text(req, req_param || client_row.fio);
        resp := UTL_HTTP.get_response(req);
        UTL_HTTP.read_text(resp, val, 1);
        UTL_HTTP.end_response(resp);
        return to_number(val);
    end;

    --������� ��� ������� ������������ ������ � �����
    function add_client_rel_room(in_client_id hotel.client.id%type, in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return hotel.client_rel_room.id%type
    is
        room_is_booked_msg CONSTANT VARCHAR2(200) := ' ����� ������������ �� ��������� ������';
        bk_exs NUMBER(20);
        room_rec hotel.room%rowtype;
        client_is_vip NUMBER(1);
        cl_rel_rm_id hotel.client_rel_room.id%type;
    begin
        bk_exs := is_room_booked(in_room_id, in_dt_bkg_from, in_dt_bkg_to);
        if bk_exs = 0 then
            --client_is_vip := get_client_status_from_api(in_client_id);
            client_is_vip := 1; --�� ����� �������, �� ������� ������� ������� ����� ������� ��� ������� ������� �� http
            insert into
                hotel.client_rel_room(client_id, room_id, date_from, date_to, flag_vip)
            values
                (in_client_id, in_room_id, in_dt_bkg_from, in_dt_bkg_to, client_is_vip)
            returning id into cl_rel_rm_id;
        else
            room_rec := get_romm_row_by_id(in_room_id);
            RAISE_APPLICATION_ERROR(-20002, room_rec.strcode || room_is_booked_msg);
        end if;
        return cl_rel_rm_id;
    end;

    --������� ���������� ��� ������� ������������� ������ � ������� ������ �� ���������� �������������� ������, �������������� ������� � ��������� ����� ������������
    function get_client_rel_room_id_by_inf(in_client_id hotel.client.id%type, in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return hotel.client_rel_room.id%type
    is
        cl_rel_rm_id hotel.client_rel_room.id%type;
    begin
        select
            id
        into
            cl_rel_rm_id
        from
            hotel.client_rel_room crm
        where
            crm.room_id = in_room_id
            and crm.client_id = in_client_id
            and crm.date_from = in_dt_bkg_from
            and crm.date_to = in_dt_bkg_to;
        return cl_rel_rm_id;
        exception
            when NO_DATA_FOUND then
                return null;
    end;

    --������� ��� ������� ������ ������������ ������ �� ��������� ������
    function del_client_rel_room(in_client_id hotel.client.id%type, in_room_id hotel.room.id%type, in_dt_bkg_from DATE, in_dt_bkg_to DATE)
    return hotel.client_rel_room.id%type 
    is
        cursor cl_rel_rm_cur(in_crm_id hotel.client_rel_room.id%type) is
            select 
                    id
                from 
                    hotel.client_rel_room
                where 
                    id = in_crm_id
                for update nowait;
        bkg_not_found_msg_p1 CONSTANT VARCHAR(100) := '��� ������ "';
        bkg_not_found_msg_p2 CONSTANT VARCHAR(100) := '" �� ���������� "';
        bkg_not_found_msg_p3 CONSTANT VARCHAR(100) := '" �� ������ � "';
        bkg_not_found_msg_p4 CONSTANT VARCHAR(100) := '" �� "';
        bkg_not_found_msg_p5 CONSTANT VARCHAR(100) := '" �� ������� ���������� � ������������ ������';
        cl_rel_rm_id hotel.client_rel_room.id%type;
        room_rec hotel.room%rowtype;
        client_rec hotel.client%rowtype;
    begin
        cl_rel_rm_id := get_client_rel_room_id_by_inf(in_client_id, in_room_id, in_dt_bkg_from, in_dt_bkg_to);
        if cl_rel_rm_id is null then
            room_rec := get_romm_row_by_id(in_room_id);
            client_rec := get_client_row_by_id(in_client_id);
            RAISE_APPLICATION_ERROR(-20001, 
                bkg_not_found_msg_p1 || room_rec.strcode || bkg_not_found_msg_p2
                || client_rec.fio || bkg_not_found_msg_p3 || in_dt_bkg_from || bkg_not_found_msg_p4 
                || in_dt_bkg_to || bkg_not_found_msg_p5
            );
        else
            for r in cl_rel_rm_cur(cl_rel_rm_id)
            loop
                delete from hotel.client_rel_room where id = r.id;
            end loop;          
        end if;
        return cl_rel_rm_id;
    end;

END PROC;
/


--
-- TBIR_CLIENT_REL_ROOM_SET_ID  (Trigger) 
--
CREATE OR REPLACE TRIGGER HOTEL.TBIR_CLIENT_REL_ROOM_SET_ID
BEFORE INSERT
ON HOTEL.CLIENT_REL_ROOM 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.id IS NULL
      )
BEGIN
    SELECT hotel.SEQCLIENT_REL_ROOM.NEXTVAL INTO :NEW.id FROM dual;
END;
/


-- 
-- Non Foreign Key Constraints for Table CLIENT 
-- 
ALTER TABLE HOTEL.CLIENT ADD (
  CONSTRAINT CLIENT_PK
  PRIMARY KEY
  (ID)
  USING INDEX HOTEL.CLIENT_PK);


-- 
-- Non Foreign Key Constraints for Table ROOM 
-- 
ALTER TABLE HOTEL.ROOM ADD (
  CONSTRAINT ROOM_PK
  PRIMARY KEY
  (ID)
  USING INDEX HOTEL.ROOM_PK);

ALTER TABLE HOTEL.ROOM ADD (
  UNIQUE (STRCODE)
  USING INDEX
    TABLESPACE USERS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));


-- 
-- Non Foreign Key Constraints for Table CLIENT_REL_ROOM 
-- 
ALTER TABLE HOTEL.CLIENT_REL_ROOM ADD (
  CONSTRAINT CLIENT_REL_ROOM_PK
  PRIMARY KEY
  (ID)
  USING INDEX HOTEL.CLIENT_REL_ROOM_PK);


-- 
-- Foreign Key Constraints for Table CLIENT_REL_ROOM 
-- 
ALTER TABLE HOTEL.CLIENT_REL_ROOM ADD (
  FOREIGN KEY (CLIENT_ID) 
  REFERENCES HOTEL.CLIENT (ID));

ALTER TABLE HOTEL.CLIENT_REL_ROOM ADD (
  FOREIGN KEY (ROOM_ID) 
  REFERENCES HOTEL.ROOM (ID));