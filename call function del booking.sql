--������ ������ ������� ��� �������� ������ � ������������ ������ �� �������������� ������
declare
    del_cl_rel_rm_id hotel.client_rel_room.id%type; 
begin
    del_cl_rel_rm_id := hotel.proc.del_client_rel_room(
        in_client_id => 3
        , in_room_id => 2
        , in_dt_bkg_from => to_date('01.06.2019')
        , in_dt_bkg_to => to_date('02.06.2019')
    );
    commit;
    dbms_output.put_line('del_cl_rel_rm_id = ' || del_cl_rel_rm_id);
end;

--������ ������ ������� ��� �������� ������ � ������������ ������
declare
    del_cl_rel_rm_id hotel.client_rel_room.id%type; 
begin
    del_cl_rel_rm_id := hotel.proc.del_client_rel_room(
        in_client_id => 1
        , in_room_id => 3 
        , in_dt_bkg_from => to_date('10.06.2019')
        , in_dt_bkg_to => to_date('17.06.2019')
    );
    commit;
    dbms_output.put_line('del_cl_rel_rm_id = ' || del_cl_rel_rm_id);
end;