--������ ������ ������� ��� ������������ ������ � ������������ ��� ������������ �� ������ ������ 
declare
    new_cl_rel_rm_id hotel.client_rel_room.id%type;    
begin
    new_cl_rel_rm_id := hotel.proc.add_client_rel_room(
        in_client_id => 3
        , in_room_id => 1 
        , in_dt_bkg_from => to_date('10.06.2019')
        , in_dt_bkg_to => to_date('17.06.2019')
    );
    commit;
    dbms_output.put_line('new_cl_rel_rm_id = ' || new_cl_rel_rm_id);
end;

--������ ������ ������� ��� ������������ ������ � ������������ ��� ������������ �� ������ ������ 
declare
    new_cl_rel_rm_id hotel.client_rel_room.id%type;    
begin
    new_cl_rel_rm_id := hotel.proc.add_client_rel_room(
        in_client_id => 2
        , in_room_id => 2        
        , in_dt_bkg_from => to_date('02.06.2019')
        , in_dt_bkg_to => to_date('09.06.2019')
    );
    commit;
    dbms_output.put_line('new_cl_rel_rm_id = ' || new_cl_rel_rm_id);
end;

--select * from hotel.room

--select * from hotel.client

--select * from hotel.client_rel_room
